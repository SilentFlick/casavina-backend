ALTER TABLE events
    ADD created_at TIMESTAMP WITHOUT TIME ZONE;

CREATE UNIQUE INDEX IX_pk_authorities ON authorities (username, authority);

ALTER TABLE events
    DROP COLUMN create_at;

DROP SEQUENCE reservation_seq CASCADE;