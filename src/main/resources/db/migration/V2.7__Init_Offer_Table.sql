CREATE TABLE offer
(
    id         VARCHAR(255)                NOT NULL,
    title      VARCHAR(255)                NOT NULL,
    message    VARCHAR(255)                NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE,
    expired_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    CONSTRAINT pk_offer PRIMARY KEY (id)
);

ALTER TABLE menu
    DROP COLUMN favorite;

DROP SEQUENCE events_seq CASCADE;

ALTER TABLE events
    DROP COLUMN id;

ALTER TABLE events
    ADD id VARCHAR(255) NOT NULL PRIMARY KEY;