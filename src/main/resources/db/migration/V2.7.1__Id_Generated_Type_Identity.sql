DROP SEQUENCE allergen_seq CASCADE;

DROP SEQUENCE category_seq CASCADE;

DROP SEQUENCE ingredient_seq CASCADE;

DROP SEQUENCE menu_seq CASCADE;

CREATE SEQUENCE IF NOT EXISTS allergen_id_seq;
ALTER TABLE allergen
    ALTER COLUMN id SET NOT NULL;
ALTER TABLE allergen
    ALTER COLUMN id SET DEFAULT nextval('allergen_id_seq');

ALTER SEQUENCE allergen_id_seq OWNED BY allergen.id;

CREATE SEQUENCE IF NOT EXISTS category_id_seq;
ALTER TABLE category
    ALTER COLUMN id SET NOT NULL;
ALTER TABLE category
    ALTER COLUMN id SET DEFAULT nextval('category_id_seq');

ALTER SEQUENCE category_id_seq OWNED BY category.id;

CREATE SEQUENCE IF NOT EXISTS ingredient_id_seq;
ALTER TABLE ingredient
    ALTER COLUMN id SET NOT NULL;
ALTER TABLE ingredient
    ALTER COLUMN id SET DEFAULT nextval('ingredient_id_seq');

ALTER SEQUENCE ingredient_id_seq OWNED BY ingredient.id;

CREATE SEQUENCE IF NOT EXISTS menu_id_seq;
ALTER TABLE menu
    ALTER COLUMN id SET NOT NULL;
ALTER TABLE menu
    ALTER COLUMN id SET DEFAULT nextval('menu_id_seq');

ALTER SEQUENCE menu_id_seq OWNED BY menu.id;