CREATE TABLE offer_price
(
    offer_id VARCHAR(255) NOT NULL,
    price    FLOAT        NOT NULL
);

ALTER TABLE offer_price
    ADD CONSTRAINT fk_offer_price_on_offer FOREIGN KEY (offer_id) REFERENCES offer (id);