CREATE TABLE reservation
(
    id               BIGINT                      NOT NULL,
    create_at        TIMESTAMP WITHOUT TIME ZONE,
    first_name       VARCHAR(255)                NOT NULL,
    last_name        VARCHAR(255)                NOT NULL,
    notice           VARCHAR(255),
    number_of_people INTEGER                     NOT NULL,
    phone            VARCHAR(255)                NOT NULL,
    reserve_at       TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    status           VARCHAR(255),
    CONSTRAINT reservation_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE IF NOT EXISTS allergen_seq AS bigint START WITH 1 INCREMENT BY 50 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE SEQUENCE IF NOT EXISTS category_seq AS bigint START WITH 1 INCREMENT BY 50 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE SEQUENCE IF NOT EXISTS events_seq AS bigint START WITH 1 INCREMENT BY 50 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE SEQUENCE IF NOT EXISTS ingredient_seq AS bigint START WITH 1 INCREMENT BY 50 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE SEQUENCE IF NOT EXISTS menu_seq AS bigint START WITH 1 INCREMENT BY 50 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE SEQUENCE IF NOT EXISTS reservation_seq AS bigint START WITH 1 INCREMENT BY 50 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE allergen
(
    id         BIGINT       NOT NULL,
    name       VARCHAR(255) NOT NULL,
    short_name VARCHAR(255) NOT NULL,
    CONSTRAINT allergen_pkey PRIMARY KEY (id)
);

CREATE TABLE authorities
(
    authority VARCHAR(255) NOT NULL,
    username  VARCHAR(255) NOT NULL,
    CONSTRAINT authorities_pkey PRIMARY KEY (authority, username)
);

CREATE TABLE category
(
    id   BIGINT       NOT NULL,
    name VARCHAR(255) NOT NULL,
    CONSTRAINT category_pkey PRIMARY KEY (id)
);

CREATE TABLE events
(
    id        BIGINT       NOT NULL,
    create_at TIMESTAMP WITHOUT TIME ZONE,
    message   VARCHAR(255) NOT NULL,
    title     VARCHAR(255) NOT NULL,
    CONSTRAINT events_pkey PRIMARY KEY (id)
);

CREATE TABLE ingredient
(
    id   BIGINT       NOT NULL,
    name VARCHAR(255) NOT NULL,
    CONSTRAINT ingredient_pkey PRIMARY KEY (id)
);

CREATE TABLE menu
(
    id          BIGINT       NOT NULL,
    description VARCHAR(255),
    image       VARCHAR(255),
    menu_id     VARCHAR(255) NOT NULL,
    name        VARCHAR(255) NOT NULL,
    category_id BIGINT       NOT NULL,
    CONSTRAINT menu_pkey PRIMARY KEY (id)
);

CREATE TABLE menu_allergens
(
    menu_id      BIGINT NOT NULL,
    allergens_id BIGINT NOT NULL
);

CREATE TABLE menu_favorite
(
    menu_id  BIGINT NOT NULL,
    favorite BIGINT NOT NULL,
    CONSTRAINT menu_favorite_pkey PRIMARY KEY (menu_id, favorite)
);

CREATE TABLE menu_ingredients
(
    menu_id        BIGINT NOT NULL,
    ingredients_id BIGINT NOT NULL
);

CREATE TABLE menu_price
(
    menu_id BIGINT NOT NULL,
    price   FLOAT4 NOT NULL
);

CREATE TABLE token
(
    refresh_token VARCHAR(255)             NOT NULL,
    activate      BOOLEAN                  NOT NULL,
    expire_at     TIMESTAMP WITH TIME ZONE NOT NULL,
    subject       VARCHAR(255)             NOT NULL,
    updated_at    TIMESTAMP WITHOUT TIME ZONE,
    CONSTRAINT token_pkey PRIMARY KEY (refresh_token)
);

CREATE TABLE users
(
    username VARCHAR(255) NOT NULL,
    enabled  BOOLEAN      NOT NULL,
    password VARCHAR(255),
    CONSTRAINT users_pkey PRIMARY KEY (username)
);

ALTER TABLE menu_ingredients
    ADD CONSTRAINT fk2417wohcu0wg90vm00ipjhm1v FOREIGN KEY (menu_id) REFERENCES menu (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE menu_favorite
    ADD CONSTRAINT fkc94hti3gm5jfqv4jro6an5dr8 FOREIGN KEY (menu_id) REFERENCES menu (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE menu_ingredients
    ADD CONSTRAINT fkd2cya6wuc2metcqyieqh507oh FOREIGN KEY (ingredients_id) REFERENCES ingredient (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE menu_allergens
    ADD CONSTRAINT fkfc6tn5rsrnllyph85fs9hi483 FOREIGN KEY (allergens_id) REFERENCES allergen (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE menu_allergens
    ADD CONSTRAINT fkfib6af1y59lbivl0tl342e3fn FOREIGN KEY (menu_id) REFERENCES menu (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE authorities
    ADD CONSTRAINT fkhjuy9y4fd8v5m3klig05ktofg FOREIGN KEY (username) REFERENCES users (username) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE menu_price
    ADD CONSTRAINT fkq1e4tgk42s5v5xptqymdkyr7h FOREIGN KEY (menu_id) REFERENCES menu (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE menu
    ADD CONSTRAINT fkww84tou7nixng06lmxawvcre FOREIGN KEY (category_id) REFERENCES category (id) ON UPDATE NO ACTION ON DELETE NO ACTION;