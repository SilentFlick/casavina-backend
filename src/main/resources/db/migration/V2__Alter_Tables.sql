ALTER TABLE menu
    ADD favorite BOOLEAN;

ALTER TABLE reservation
    ADD created_at TIMESTAMP WITHOUT TIME ZONE;

ALTER TABLE reservation
    ADD modified_at TIMESTAMP WITHOUT TIME ZONE;

ALTER TABLE menu_favorite
    DROP CONSTRAINT fkc94hti3gm5jfqv4jro6an5dr8;

ALTER TABLE reservation
    DROP COLUMN create_at;

ALTER TABLE reservation
    ALTER COLUMN id TYPE VARCHAR(255) USING (id::VARCHAR(255));