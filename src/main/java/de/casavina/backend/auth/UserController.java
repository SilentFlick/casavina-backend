package de.casavina.backend.auth;

import de.casavina.backend.model.Users;
import jakarta.validation.Valid;
import jakarta.validation.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


@RestController
@CrossOrigin
@RequestMapping(value = "/v1/users", produces = "application/json")
public class UserController {
	private final UserRepository userRepository;
	private final TokenRepository tokenRepository;
	private final UserDetailsManager userDetailsManager;
	private final PasswordEncoder passwordEncoder;
	private final AuthenticationManager authenticationManager;

	public UserController(UserRepository userRepository , TokenRepository tokenRepository ,
	                      UserDetailsManager userDetailsManager ,
	                      PasswordEncoder passwordEncoder , AuthenticationManager authenticationManager) {
		this.userRepository = userRepository;
		this.tokenRepository = tokenRepository;
		this.userDetailsManager = userDetailsManager;
		this.passwordEncoder = passwordEncoder;
		this.authenticationManager = authenticationManager;
	}

	@GetMapping
	public Iterable<Users> findAllUsers() {
		return userRepository.findAll( );
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void createUser(@Valid @RequestBody Users user) {
		try {
			UserDetails newUser = User.builder( )
					.username( user.getUsername( ) )
					.password( passwordEncoder.encode( user.getPassword( ) ) )
					.roles( "USER" )
					.build( );
			userDetailsManager.createUser( newUser );
		} catch (ValidationException e) {
			throw new ResponseStatusException( HttpStatus.BAD_REQUEST , "Invalid user data" , e );
		}
	}

	@PatchMapping
	public void changePassword(@RequestBody PasswordDTO passwordDTO) {
		if ( !userDetailsManager.userExists( passwordDTO.username( ) ) ) {
			throw new ResponseStatusException( HttpStatus.NOT_FOUND , "user not existed" );
		}
		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
				new UsernamePasswordAuthenticationToken( passwordDTO.username( ) , passwordDTO.oldPassword( )
				);
		Authentication authentication = authenticationManager.authenticate( usernamePasswordAuthenticationToken );
		if ( authentication.isAuthenticated( ) ) {
			userDetailsManager.deleteUser( passwordDTO.username( ) );
			UserDetails newUser = User.builder( )
					.username( passwordDTO.username( ) )
					.password( passwordEncoder.encode( passwordDTO.newPassword( ) ) )
					.authorities( authentication.getAuthorities( ) )
					.build( );
			userDetailsManager.createUser( newUser );
			tokenRepository.deleteAllBySubject( passwordDTO.username( ) );
		} else {
			throw new ResponseStatusException( HttpStatus.UNAUTHORIZED , "Invalid username or old password" );
		}
	}

	@DeleteMapping("/{username}")
	public void deleteUserByUsername(@PathVariable String username) {
		userDetailsManager.deleteUser( username );
		tokenRepository.deleteAllBySubject( username );
	}
}
