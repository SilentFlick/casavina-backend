package de.casavina.backend.auth;

public record PasswordDTO(
		String username ,
		String oldPassword ,
		String newPassword
) {
}
