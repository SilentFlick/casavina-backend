package de.casavina.backend.auth;

import de.casavina.backend.model.Token;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;

@Transactional
public interface TokenRepository extends JpaRepository<Token, String> {
	void deleteAllBySubject(String subject);

	void deleteByExpireAtBefore(Instant expireAt);
}
