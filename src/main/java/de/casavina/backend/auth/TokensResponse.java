package de.casavina.backend.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.Instant;

public record TokensResponse(
		String accessToken ,
		Instant accessExpire ,
		@JsonIgnore String refreshToken ,
		@JsonIgnore Instant refreshExpire) {
}