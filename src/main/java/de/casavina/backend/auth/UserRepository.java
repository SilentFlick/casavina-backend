package de.casavina.backend.auth;

import de.casavina.backend.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<Users, String> {
	Optional<Users> findByUsernameAndEnabled(String username , Boolean enabled);
}
