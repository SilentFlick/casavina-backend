package de.casavina.backend.auth;

import de.casavina.backend.model.Users;
import de.casavina.backend.security.BruteForceProtectionService;
import de.casavina.backend.security.CookieProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.Duration;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping(value = "/v1/auth", produces = "application/json")
public class AuthenticationController {
	private final TokenService tokenService;
	private final AuthenticationManager authenticationManager;
	private final UserRepository userRepository;
	private final BruteForceProtectionService bruteForceProtectionService;
	private final JwtDecoder jwtDecoder;
	private final CookieProperties cookieProperties;

	public AuthenticationController(
			TokenService tokenService ,
			AuthenticationManager authenticationManager ,
			UserRepository userRepository ,
			BruteForceProtectionService bruteForceProtectionService , JwtDecoder jwtDecoder ,
			CookieProperties cookieProperties) {
		this.tokenService = tokenService;
		this.authenticationManager = authenticationManager;
		this.userRepository = userRepository;
		this.bruteForceProtectionService = bruteForceProtectionService;
		this.jwtDecoder = jwtDecoder;
		this.cookieProperties = cookieProperties;
	}

	@PostMapping("/login")
	public ResponseEntity<TokensResponse> login(@RequestBody Users loginRequest) {
		Users user = userRepository.findByUsernameAndEnabled( loginRequest.getUsername( ) , true )
				.orElseThrow( () -> new ResponseStatusException( HttpStatus.UNAUTHORIZED ,
						"Username not found or disabled" ) );
		if ( bruteForceProtectionService.isClientBlocked( ) ) {
			user.setEnabled( false );
			userRepository.save( user );
			log.info( "Brute force detecting, disabling user " + user.getUsername( ) );
			throw new ResponseStatusException( HttpStatus.UNAUTHORIZED ,
					"Brute force attempt detecting" );
		}
		try {
			UsernamePasswordAuthenticationToken authenticationToken =
					UsernamePasswordAuthenticationToken.unauthenticated(
							loginRequest.getUsername( ) , loginRequest.getPassword( ) );
			Authentication authentication =
					authenticationManager.authenticate( authenticationToken );
			TokensResponse tokens = tokenService.generateTokens( authentication );
			return getTokensResponseResponseEntity( tokens );
		} catch (AuthenticationException ae) {
			log.info( "Failed to authenticate: '" + loginRequest.getUsername( ) + "'. " +
					ae.getMessage( ) );
			throw new ResponseStatusException( HttpStatus.UNAUTHORIZED , "Invalid credentials" ,
					ae );
		}
	}

	@PostMapping("/renew")
	public ResponseEntity<TokensResponse> renewAccessToken(
			@CookieValue(name = "refresh-token") String refreshToken ,
			@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization) {
		try {
			String jwtToken = authorization.substring( 7 );
			Jwt jwt = jwtDecoder.decode( jwtToken );
			TokensResponse tokens =
					tokenService.renewTokens( refreshToken , jwt.getSubject( ) , jwt.getClaim( "scope" ) );
			return getTokensResponseResponseEntity( tokens );
		} catch (InvalidTokenException je) {
			log.info( je.getMessage( ) );
			throw new ResponseStatusException( HttpStatus.UNAUTHORIZED , "Invalid tokens" + je );
		}
	}

	private ResponseEntity<TokensResponse> getTokensResponseResponseEntity(TokensResponse tokens) {
		ResponseCookie refreshTokenCookie =
				ResponseCookie.from( "refresh-token" , tokens.refreshToken( ) )
						.httpOnly( true )
						.secure( true )
						.domain( cookieProperties.domain( ) )
						.path( "/v1/auth" )
						.maxAge( Duration.ofHours( 8 ) )
						.build( );
		ResponseCookie accessTokenCookie =
				ResponseCookie.from( "access-token" , tokens.accessToken( ) )
						.secure( true )
						.domain( cookieProperties.domain( ) )
						.path( "/" )
						.maxAge( Duration.ofMinutes( 30 ) )
						.build( );
		return ResponseEntity.ok( )
				.header( HttpHeaders.SET_COOKIE , accessTokenCookie.toString( ) )
				.header( HttpHeaders.SET_COOKIE , refreshTokenCookie.toString( ) )
				.body( tokens );
	}

	@PostMapping("/logout")
	public ResponseEntity<Void> logout(@CookieValue(name = "refresh-token") String refreshToken) {
		tokenService.deleteToken( refreshToken );
		ResponseCookie refreshTokenCookie = ResponseCookie.from( "refresh-token" , "" )
				.httpOnly( true )
				.secure( true )
				.domain( cookieProperties.domain( ) )
				.path( "/v1/auth" )
				.maxAge( 0 )
				.build( );
		ResponseCookie accessTokenCookie = ResponseCookie.from( "access-token" , "" )
				.secure( true )
				.domain( cookieProperties.domain( ) )
				.path( "/" )
				.maxAge( 0 )
				.build( );
		return ResponseEntity.ok( )
				.header( HttpHeaders.SET_COOKIE , refreshTokenCookie.toString( ) )
				.header( HttpHeaders.SET_COOKIE , accessTokenCookie.toString( ) )
				.build( );
	}

}
