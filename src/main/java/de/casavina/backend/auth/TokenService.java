package de.casavina.backend.auth;

import de.casavina.backend.model.Token;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TokenService {
	private final JwtEncoder jwtEncoder;
	private final TokenRepository tokenRepository;

	public TokenService(JwtEncoder jwtEncoder , TokenRepository tokenRepository) {
		this.jwtEncoder = jwtEncoder;
		this.tokenRepository = tokenRepository;
	}

	private TokensResponse getNewTokens(String subject , String scope) {
		Instant expire = Instant.now( ).plus( 30 , ChronoUnit.MINUTES );
		JwtClaimsSet claimsSet = JwtClaimsSet.builder( )
				.issuer( "self" )
				.issuedAt( Instant.now( ) )
				.expiresAt( expire )
				.subject( subject )
				.claim( "scope" , scope )
				.build( );
		String accessToken =
				jwtEncoder.encode( JwtEncoderParameters.from( claimsSet ) ).getTokenValue( );
		Instant refreshExpireAt = expire.plus( 8 , ChronoUnit.HOURS );
		Token token = new Token( );
		token.setExpireAt( refreshExpireAt );
		token.setActivate( true );
		token.setSubject( subject );
		tokenRepository.save( token );
		return new TokensResponse( accessToken , expire , token.getRefreshToken( ) , refreshExpireAt );
	}

	public TokensResponse generateTokens(Authentication authentication) {
		String scope = authentication.getAuthorities( ).stream( )
				.map( GrantedAuthority::getAuthority )
				.collect( Collectors.joining( " " ) );
		return this.getNewTokens( authentication.getName( ) , scope );
	}

	public TokensResponse renewTokens(String refreshToken , String subject , String scope) {
		Token currentToken = tokenRepository.findById( refreshToken )
				.orElseThrow( () -> new InvalidTokenException( "Refresh token not existed" ) );
		int recentlyUpdated =
				currentToken.getUpdatedAt( ).minusSeconds( 10 ).compareTo( LocalDateTime.now( ) );
		if ( !currentToken.getActivate( ) && recentlyUpdated > 0 ) {
			tokenRepository.deleteAllBySubject( currentToken.getSubject( ) );
			throw new InvalidTokenException( "Detect reuse token" );
		} else {
			Instant refreshExpireAt = currentToken.getExpireAt( );
			if ( refreshExpireAt.compareTo( Instant.now( ) ) > 0 ) {
				currentToken.setActivate( false );
				tokenRepository.save( currentToken );
				return this.getNewTokens( subject , scope );
			}
			throw new InvalidTokenException( "Refresh token expired" );
		}
	}

	public void deleteToken(String refreshToken) {
		tokenRepository.deleteById( refreshToken );
	}
}
