package de.casavina.backend.events;

import de.casavina.backend.model.Events;
import jakarta.validation.Valid;
import jakarta.validation.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin
@RequestMapping(value = "/v1/events", produces = "application/json")
public class EventsController {
	private final EventsRepository eventsRepository;

	public EventsController(EventsRepository eventsRepository) {
		this.eventsRepository = eventsRepository;
	}

	@GetMapping
	public Iterable<Events> findAllEvents() {
		return eventsRepository.findAllByOrderByCreatedAtDesc( );
	}

	@GetMapping("/{id}")
	public Events getEventById(@PathVariable String id) {
		return eventsRepository.findById( id ).orElseThrow(
				() -> new ResponseStatusException( HttpStatus.NOT_FOUND , "event not existed" ) );
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Events createEvent(@Valid @RequestBody Events event) {
		try {
			return eventsRepository.save( event );
		} catch (ValidationException e) {
			throw new ResponseStatusException( HttpStatus.BAD_REQUEST , "Invalid event data" , e );
		}
	}

	@PutMapping
	public void updateEvent(@Valid @RequestBody Events event) {
		try {
			Events existingEvent = eventsRepository.findById( event.getId( ) )
					.orElseThrow( () -> new ResponseStatusException( HttpStatus.NOT_FOUND ,
							"Event " + event.getId( ) + " not found" ) );
			existingEvent.setTitle( event.getTitle( ) );
			existingEvent.setMessage( event.getMessage( ) );
			existingEvent.setExpiredAt( event.getExpiredAt( ) );
			eventsRepository.save( existingEvent );
		} catch (ValidationException e) {
			throw new ResponseStatusException( HttpStatus.BAD_REQUEST , "Invalid event data" , e );
		}
	}

	@DeleteMapping("/{id}")
	public void deleteEvent(@PathVariable String id) {
		eventsRepository.deleteById( id );
	}

}
