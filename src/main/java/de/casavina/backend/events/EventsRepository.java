package de.casavina.backend.events;

import de.casavina.backend.model.Events;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;

@Transactional
public interface EventsRepository extends JpaRepository<Events, String> {
	Iterable<Events> findAllByOrderByCreatedAtDesc();

	void deleteEventsByExpiredAtBefore(Instant expiredAt);
}
