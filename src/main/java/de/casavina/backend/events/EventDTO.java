package de.casavina.backend.events;

import java.time.Instant;

public record EventDTO(String title , String message , Instant expiredAt) {
}
