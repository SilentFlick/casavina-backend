package de.casavina.backend.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ReservationTimeValidator.class)
@Target({ElementType.METHOD , ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ReservationTimeConstraint {
	String message() default "Invalid reservation date and time";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
