package de.casavina.backend.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.List;

public class PriceValidator implements ConstraintValidator<PriceConstraint, List<Float>> {
	@Override
	public boolean isValid(List<Float> prices , ConstraintValidatorContext context) {
		if ( !prices.isEmpty( ) && prices.size( ) <= 3 ) {
			for (Float price : prices) {
				if ( price == null || price < 0.1 ) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
}
