package de.casavina.backend.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class ReservationTimeValidator
		implements ConstraintValidator<ReservationTimeConstraint, LocalDateTime> {
	@Override
	public boolean isValid(LocalDateTime reserveAt , ConstraintValidatorContext context) {
		LocalDateTime minReserve = LocalDateTime.now( ).plusDays( 2 );
		LocalDateTime maxReserve = LocalDateTime.now( ).plusYears( 3 );
		return reserveAt.isAfter( minReserve ) && reserveAt.isBefore( maxReserve ) && isTimeValid( reserveAt );
	}

	private boolean isTimeValid(LocalDateTime reserveAt) {
		DayOfWeek dayOfWeek = reserveAt.getDayOfWeek( );
		LocalTime time = reserveAt.toLocalTime( );
		if ( dayOfWeek != DayOfWeek.MONDAY && dayOfWeek != DayOfWeek.TUESDAY ) {
			return switch (dayOfWeek) {
				case WEDNESDAY , THURSDAY -> (time.isAfter( LocalTime.of( 11 , 29 ) ) &&
						time.isBefore( LocalTime.of( 13 , 59 ) )) ||
						(time.isAfter( LocalTime.of( 17 , 29 ) ) &&
								time.isBefore( LocalTime.of( 21 , 59 ) ));
				case FRIDAY , SATURDAY -> (time.isAfter( LocalTime.of( 11 , 29 ) ) &&
						time.isBefore( LocalTime.of( 13 , 59 ) )) ||
						(time.isAfter( LocalTime.of( 17 , 29 ) ));
				case SUNDAY -> (time.isAfter( LocalTime.of( 11 , 29 ) ) &&
						time.isBefore( LocalTime.of( 13 , 59 ) )) ||
						(time.isAfter( LocalTime.of( 16 , 59 ) ) &&
								time.isBefore( LocalTime.of( 20 , 59 ) ));
				default -> false;
			};
		}
		return false;
	}
}
