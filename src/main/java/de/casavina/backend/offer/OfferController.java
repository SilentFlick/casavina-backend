package de.casavina.backend.offer;

import de.casavina.backend.model.Offer;
import jakarta.validation.Valid;
import jakarta.validation.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin
@RequestMapping(value = "/v1/offers", produces = "application/json")
public class OfferController {
	private final OfferRepository offerRepository;

	public OfferController(OfferRepository offerRepository) {
		this.offerRepository = offerRepository;
	}

	@GetMapping
	public Iterable<Offer> findAllOffers() {
		return offerRepository.findAll( );
	}

	@GetMapping("/{id}")
	public Offer findOfferById(@PathVariable String id) {
		return offerRepository.findById( id )
				.orElseThrow( () -> new ResponseStatusException( HttpStatus.NOT_FOUND , "Offer not existed" ) );
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Offer createOffer(@Valid @RequestBody Offer offer) {
		try {
			return offerRepository.save( offer );
		} catch (ValidationException e) {
			throw new ResponseStatusException( HttpStatus.BAD_REQUEST , "Invalid offer data" , e );
		}
	}

	@PutMapping
	public void updateOffer(@Valid @RequestBody Offer offer) {
		try {
			offerRepository.findById( offer.getId( ) )
					.map( existingOffer -> {
						existingOffer.setTitle( offer.getTitle( ) );
						existingOffer.setMessage( offer.getMessage( ) );
						existingOffer.setExpiredAt( offer.getExpiredAt( ) );
						existingOffer.setPrice( offer.getPrice( ) );
						return offerRepository.save( existingOffer );
					} )
					.orElseThrow( () -> new ResponseStatusException( HttpStatus.NOT_FOUND ,
							"Offer " + offer.getId( ) + " not found" ) );
		} catch (ValidationException e) {
			throw new ResponseStatusException( HttpStatus.BAD_REQUEST , "Invalid offer data" , e );
		}
	}

	@DeleteMapping("/{id}")
	public void deleteOffer(@PathVariable String id) {
		offerRepository.deleteById( id );
	}
}
