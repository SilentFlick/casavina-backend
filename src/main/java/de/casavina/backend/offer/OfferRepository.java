package de.casavina.backend.offer;

import de.casavina.backend.model.Offer;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

@Transactional
public interface OfferRepository extends JpaRepository<Offer, String> {
}
