package de.casavina.backend.reservation;

import de.casavina.backend.security.NtfyProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class ReservationNotifyService {
	private final NtfyProperties ntfyProperties;
	private final RestTemplate restTemplate = new RestTemplate( );

	public ReservationNotifyService(NtfyProperties ntfyProperties) {
		this.ntfyProperties = ntfyProperties;
	}

	@Async
	public void publish(String title , String message) {
		HttpHeaders httpHeaders = new HttpHeaders( );
		httpHeaders.add( "Title" , title );
		httpHeaders.add( "Authorization" , "Bearer " + ntfyProperties.token( ) );
		HttpEntity<String> httpEntity = new HttpEntity<>( message , httpHeaders );
		restTemplate.postForObject( ntfyProperties.host( ) + "/Reservierung" , httpEntity , String.class );
		log.info( "Publish a new message with title to " + ntfyProperties.host( ) + "/Reservierung" );
	}
}
