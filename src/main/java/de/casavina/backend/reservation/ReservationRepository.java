package de.casavina.backend.reservation;

import de.casavina.backend.model.Reservation;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;


@Transactional
public interface ReservationRepository extends JpaRepository<Reservation, String> {
	Iterable<Reservation> findReservationsByReserveAtBetweenOrderByReserveAtAsc(LocalDateTime start ,
	                                                                            LocalDateTime end);

	void deleteByReserveAtBefore(LocalDateTime reserveAt);
}
