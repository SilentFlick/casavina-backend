package de.casavina.backend.reservation;

import de.casavina.backend.model.Reservation;
import jakarta.validation.Valid;
import jakarta.validation.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping(value = "/v1/reservations", produces = "application/json")
public class ReservationController {
	private final ReservationRepository reservationRepository;
	private final ReservationNotifyService reservationNotifyService;

	public ReservationController(ReservationRepository reservationRepository ,
	                             ReservationNotifyService reservationNotifyService) {
		this.reservationRepository = reservationRepository;
		this.reservationNotifyService = reservationNotifyService;
	}

	@GetMapping public Iterable<Reservation> findAllReservations() {
		LocalDateTime start = LocalDateTime.now( ).plusDays( 2 );
		LocalDateTime end = LocalDateTime.now( ).plusYears( 3 );
		return reservationRepository.findReservationsByReserveAtBetweenOrderByReserveAtAsc( start , end );
	}

	@GetMapping("/{id}") public Reservation findReservationById(@PathVariable String id) {
		return reservationRepository.findById( id ).orElseThrow(
				() -> new ResponseStatusException( HttpStatus.NOT_FOUND , "Reservation not existed" ) );
	}

	@PostMapping @ResponseStatus(HttpStatus.CREATED)
	public Map<String, String> createReservation(@Valid @RequestBody Reservation reservation) {
		try {
			String message =
					"We've got a fresh booking for " + reservation.getNumberOfPeople( ) + " people scheduled at " + reservation.getReserveAt( );
			reservationNotifyService.publish( "New reservation!" , message );
			Reservation createdReservation = reservationRepository.save( reservation );
			return Collections.singletonMap( "id" , createdReservation.getId( ) );
		} catch (ValidationException e) {
			throw new ResponseStatusException( HttpStatus.BAD_REQUEST , "Invalid reservation data" , e );
		}
	}

	@PutMapping public void updateReservation(@Valid @RequestBody Reservation reservation) {
		try {
			Reservation existingReservation = reservationRepository.findById( reservation.getId( ) ).orElseThrow(
					() -> new ResponseStatusException( HttpStatus.NOT_FOUND , "Reservation not existed" ) );
			existingReservation.setFirstName( reservation.getFirstName( ) );
			existingReservation.setLastName( reservation.getLastName( ) );
			existingReservation.setPhone( reservation.getPhone( ) );
			existingReservation.setNumberOfPeople( reservation.getNumberOfPeople( ) );
			existingReservation.setReserveAt( reservation.getReserveAt( ) );
			existingReservation.setNotice( reservation.getNotice( ) );
			existingReservation.setStatus( reservation.getStatus( ) );
			reservationRepository.save( existingReservation );
		} catch (ValidationException e) {
			throw new ResponseStatusException( HttpStatus.BAD_REQUEST , "Invalid reservation data" , e );
		}
	}
}
