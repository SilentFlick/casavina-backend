package de.casavina.backend;

import de.casavina.backend.security.CookieProperties;
import de.casavina.backend.security.CorsConfigurationProperties;
import de.casavina.backend.security.NtfyProperties;
import de.casavina.backend.security.RsaKeyProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableConfigurationProperties(
		{RsaKeyProperties.class , CorsConfigurationProperties.class ,
				CookieProperties.class , NtfyProperties.class})
@EnableJpaAuditing
@EnableTransactionManagement
@SpringBootApplication
public class BackendApplication {

	public static void main(String[] args) {
		SpringApplication.run( BackendApplication.class , args );
	}

}
