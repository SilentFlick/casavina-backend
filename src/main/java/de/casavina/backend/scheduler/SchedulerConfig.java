package de.casavina.backend.scheduler;

import de.casavina.backend.auth.TokenRepository;
import de.casavina.backend.events.EventsRepository;
import de.casavina.backend.reservation.ReservationRepository;
import de.casavina.backend.security.BruteForceProtectionService;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableScheduling
public class SchedulerConfig {
	private final TokenRepository tokenRepository;
	private final ReservationRepository reservationRepository;
	private final EventsRepository eventsRepository;
	private final BruteForceProtectionService bruteForceProtectionService;

	public SchedulerConfig(TokenRepository tokenRepository ,
	                       ReservationRepository reservationRepository ,
	                       EventsRepository eventsRepository ,
	                       BruteForceProtectionService bruteForceProtectionService) {
		this.tokenRepository = tokenRepository;
		this.reservationRepository = reservationRepository;
		this.eventsRepository = eventsRepository;
		this.bruteForceProtectionService = bruteForceProtectionService;
	}

	@Scheduled(fixedRate = 8, timeUnit = TimeUnit.HOURS)
	public void deleteOldTokens() {
		tokenRepository.deleteByExpireAtBefore( Instant.now( ) );
	}

	@Scheduled(fixedRate = 1, timeUnit = TimeUnit.DAYS)
	public void deleteOldReservations() {
		reservationRepository.deleteByReserveAtBefore( LocalDateTime.now( ) );
		eventsRepository.deleteEventsByExpiredAtBefore( Instant.now( ) );
	}

	@Scheduled(fixedRate = 12, timeUnit = TimeUnit.HOURS)
	public void deleteOldBlockedClients() {
		bruteForceProtectionService.clearAttemptCache( );
	}
}
