package de.casavina.backend.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@Data
@RequiredArgsConstructor
public class AuthorityId implements Serializable {
	private Users username;

	private String authority;
}
