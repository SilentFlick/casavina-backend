package de.casavina.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@IdClass(AuthorityId.class)
@Table(name = "authorities")
@Entity
public class Authority {
	@Id
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "username", nullable = false)
	private Users username;

	@Id
	private String authority;
}
