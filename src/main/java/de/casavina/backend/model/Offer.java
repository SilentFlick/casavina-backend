package de.casavina.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import de.casavina.backend.validation.PriceConstraint;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Offer {
	@Id
	@GeneratedValue(strategy = GenerationType.UUID)
	private String id;

	@NotNull(message = "title is mandatory")
	private String title;

	@Lob
	@NotNull(message = "message is mandatory")
	private String message;

	@CreatedDate
	private Instant createdAt;

	@NotNull(message = "expiredAt is mandatory")
	private Instant expiredAt;

	@ElementCollection(targetClass = Float.class, fetch = FetchType.EAGER)
	@Column(nullable = false)
	@PriceConstraint
	private List<Float> price;
}
