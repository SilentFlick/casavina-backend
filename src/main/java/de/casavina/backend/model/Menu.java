package de.casavina.backend.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.casavina.backend.validation.PriceConstraint;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Menu {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "category_id", nullable = false)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonIdentityReference(alwaysAsId = true)
	private Category category;

	@Column(nullable = false)
	private String name;

	@ManyToMany
	@JoinColumn(referencedColumnName = "id")
	@ToString.Exclude
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonIdentityReference(alwaysAsId = true)
	private List<Ingredient> ingredients;

	private String description;

	@ManyToMany
	@JoinColumn(referencedColumnName = "id")
	@ToString.Exclude
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonIdentityReference(alwaysAsId = true)
	private List<Allergen> allergens;

	@ElementCollection(targetClass = Float.class, fetch = FetchType.EAGER)
	@Column(nullable = false)
	@PriceConstraint
	private List<Float> price;

	private String image;
}
