package de.casavina.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Token {
	@Id
	@GeneratedValue(strategy = GenerationType.UUID)
	private String refreshToken;

	@Column(nullable = false)
	private Instant expireAt;

	@Column(nullable = false)
	private Boolean activate;

	@Column(nullable = false)
	private String subject;

	private LocalDateTime updatedAt;

	@PrePersist
	@PreUpdate
	void activate() {
		this.updatedAt = LocalDateTime.now( );
	}
}
