package de.casavina.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import de.casavina.backend.validation.ReservationTimeConstraint;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Reservation {
	@Id
	@GeneratedValue(strategy = GenerationType.UUID)
	private String id;

	@NotNull(message = "first name is mandatory")
	private String firstName;

	@NotNull(message = "last name is mandatory")
	private String lastName;

	@NotNull(message = "phone is mandatory")
	private String phone;

	@NotNull(message = "number of people is mandatory")
	private Integer numberOfPeople;

	@NotNull(message = "date and time are mandatory")
	@ReservationTimeConstraint
	private LocalDateTime reserveAt;

	private String notice;

	@Enumerated(EnumType.STRING)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Status status;

	@CreatedDate
	@JsonIgnore
	private LocalDateTime createdAt;

	@LastModifiedDate
	@JsonIgnore
	private LocalDateTime modifiedAt;

	@PrePersist
	void prePersist() {
		this.status = Status.OPEN;
	}

	@SuppressWarnings("unused")
	public enum Status {
		OPEN,
		CLOSED
	}
}
