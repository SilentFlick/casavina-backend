package de.casavina.backend.core.menu;

import de.casavina.backend.core.allergen.AllergenRepository;
import de.casavina.backend.core.category.CategoryRepository;
import de.casavina.backend.core.ingredient.IngredientRepository;
import de.casavina.backend.model.Allergen;
import de.casavina.backend.model.Category;
import de.casavina.backend.model.Ingredient;
import de.casavina.backend.model.Menu;
import jakarta.validation.Valid;
import jakarta.validation.ValidationException;
import jakarta.validation.constraints.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping(value = "/v1/menus", produces = "application/json")
public class MenuController {
	private final MenuRepository menuRepository;
	private final CategoryRepository categoryRepository;
	private final IngredientRepository ingredientRepository;
	private final AllergenRepository allergenRepository;

	public MenuController(MenuRepository menuRepository , CategoryRepository categoryRepository ,
	                      IngredientRepository ingredientRepository ,
	                      AllergenRepository allergenRepository) {
		this.menuRepository = menuRepository;
		this.categoryRepository = categoryRepository;
		this.ingredientRepository = ingredientRepository;
		this.allergenRepository = allergenRepository;
	}

	@GetMapping
	public Iterable<Menu> findAllMenus() {
		return menuRepository.findAll( );
	}

	@GetMapping("/{id}")
	public Menu findMenuById(@PathVariable Long id) {
		return menuRepository.findById( id )
				.orElseThrow( () -> new ResponseStatusException( HttpStatus.NOT_FOUND ,
						"Menu " + id + " not existed" ) );
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Map<String, Long> createMenu(@Valid @RequestBody MenuDTO menuDTO) {
		try {
			Menu menu = getMenuFromMenuDto( menuDTO );
			Menu savedMenu = menuRepository.save( menu );
			return Collections.singletonMap( "id" , savedMenu.getId( ) );
		} catch (ValidationException e) {
			throw new ResponseStatusException( HttpStatus.BAD_REQUEST , "Invalid menu data" , e );
		}
	}

	@PutMapping("/{id}")
	public void updateMenu(@Valid @RequestBody MenuDTO menuDTO , @PathVariable Long id) {
		try {
			Menu menu = getMenuFromMenuDto( menuDTO , id );
			menuRepository.save( menu );
		} catch (ValidationException e) {
			throw new ResponseStatusException( HttpStatus.BAD_REQUEST , "Invalid menu data" , e );
		}
	}

	@DeleteMapping("/{id}")
	public void deleteMenuById(@PathVariable Long id) {
		menuRepository.deleteById( id );
	}

	private Menu getMenuFromMenuDto(@NotNull MenuDTO menuDTO) {
		return getMenuFromMenuDto( menuDTO , null );
	}

	private Menu getMenuFromMenuDto(@NotNull MenuDTO menuDTO , Long id) {
		Category category = getCategoryById( menuDTO );
		List<Ingredient> ingredients = getIngredientsFromIds( menuDTO );
		List<Allergen> allergens = getAllergensFromIds( menuDTO );
		Menu menu = (id != null) ? getMenuById( id ) : new Menu( );
		menu.setCategory( category );
		menu.setName( menuDTO.name( ) );
		menu.setIngredients( ingredients );
		menu.setDescription( menuDTO.description( ) );
		menu.setAllergens( allergens );
		menu.setPrice( menuDTO.price( ) );
		menu.setImage( menuDTO.image( ) );
		return menu;
	}

	private List<Allergen> getAllergensFromIds(@NotNull MenuDTO menuDTO) {
		List<Allergen> allergens = new ArrayList<>( );
		for (Long allergenId : menuDTO.allergens( )) {
			Allergen allergen = allergenRepository.findById( allergenId )
					.orElseThrow( () -> new ResponseStatusException( HttpStatus.NOT_FOUND ,
							"Allergen " + allergenId + " not found" ) );
			allergens.add( allergen );
		}
		return allergens;
	}

	private List<Ingredient> getIngredientsFromIds(@NotNull MenuDTO menuDTO) {
		List<Ingredient> ingredients = new ArrayList<>( );
		for (Long ingredientId : menuDTO.ingredients( )) {
			Ingredient ingredient = ingredientRepository.findById( ingredientId )
					.orElseThrow( () -> new ResponseStatusException( HttpStatus.NOT_FOUND ,
							"Ingredient " + ingredientId + " not found" ) );
			ingredients.add( ingredient );
		}
		return ingredients;
	}

	private Category getCategoryById(@NotNull MenuDTO menuDTO) {
		return categoryRepository.findById( menuDTO.category( ) )
				.orElseThrow( () -> new ResponseStatusException( HttpStatus.NOT_FOUND , "Category not found" ) );
	}

	private Menu getMenuById(Long id) {
		return menuRepository.findById( id )
				.orElseThrow( () -> new ResponseStatusException( HttpStatus.NOT_FOUND , "Menu not found" ) );
	}
}
