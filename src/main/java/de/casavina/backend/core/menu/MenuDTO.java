package de.casavina.backend.core.menu;

import java.util.List;

public record MenuDTO(
		Long category ,
		String name ,
		List<Long> ingredients ,
		String description ,
		List<Long> allergens ,
		List<Float> price ,
		String image
) {
}
