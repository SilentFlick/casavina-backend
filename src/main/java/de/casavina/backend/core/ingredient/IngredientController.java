package de.casavina.backend.core.ingredient;

import de.casavina.backend.model.Ingredient;
import jakarta.validation.Valid;
import jakarta.validation.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping(value = "/v1/ingredients", produces = "application/json")
public class IngredientController {
	private final IngredientRepository ingredientRepository;

	public IngredientController(IngredientRepository ingredientRepository) {
		this.ingredientRepository = ingredientRepository;
	}

	@GetMapping
	public Iterable<Ingredient> getAllIngredients() {
		return ingredientRepository.findAll( );
	}

	@GetMapping("/{id}")
	public Ingredient getIngredientById(@PathVariable Long id) {
		return ingredientRepository.findById( id )
				.orElseThrow(
						() -> new ResponseStatusException( HttpStatus.NOT_FOUND , "Ingredient " + id + " not found" ) );
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Map<String, Long> createIngredient(@RequestBody Ingredient ingredient) {
		Ingredient savedIngredient = ingredientRepository.save( ingredient );
		return Collections.singletonMap( "id" , savedIngredient.getId( ) );
	}

	@PutMapping
	public void updateIngredient(@Valid @RequestBody Ingredient ingredient) {
		try {
			ingredientRepository.findById( ingredient.getId( ) )
					.map( existingIngredient -> {
						existingIngredient.setName( ingredient.getName( ) );
						return ingredientRepository.save( existingIngredient );
					} )
					.orElseThrow( () -> new ResponseStatusException( HttpStatus.NOT_FOUND ,
							"Ingredient " + ingredient.getId( ) + " not found" ) );
		} catch (ValidationException e) {
			throw new ResponseStatusException( HttpStatus.BAD_REQUEST , "Invalid ingredient data" , e );
		}
	}

	@DeleteMapping("/{id}")
	public void deleteIngredient(@PathVariable Long id) {
		ingredientRepository.deleteById( id );
	}
}
