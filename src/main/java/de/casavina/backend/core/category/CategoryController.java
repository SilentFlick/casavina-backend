package de.casavina.backend.core.category;

import de.casavina.backend.model.Category;
import jakarta.validation.Valid;
import jakarta.validation.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping(value = "/v1/categories", produces = "application/json")
public class CategoryController {
	private final CategoryRepository categoryRepository;

	public CategoryController(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	@GetMapping
	public Iterable<Category> findAll() {
		return categoryRepository.findAll( );
	}

	@GetMapping("/{id}")
	public Category findCategoryById(@PathVariable Long id) {
		return categoryRepository.findById( id )
				.orElseThrow( () -> new ResponseStatusException( HttpStatus.NOT_FOUND ,
						"Category " + id + " not found" ) );
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Map<String, Long> createCategory(@RequestBody Category category) {
		Category savedCategory = categoryRepository.save( category );
		return Collections.singletonMap( "id" , savedCategory.getId( ) );
	}

	@PutMapping
	public void updateCategory(@Valid @RequestBody Category category) {
		try {
			categoryRepository.findById( category.getId( ) )
					.map( existingCategory -> {
						existingCategory.setName( category.getName( ) );
						existingCategory.setMenus( category.getMenus( ) );
						return categoryRepository.save( existingCategory );
					} )
					.orElseThrow( () -> new ResponseStatusException( HttpStatus.NOT_FOUND ,
							"Category " + category.getId( ) + " not found" ) );
		} catch (ValidationException e) {
			throw new ResponseStatusException( HttpStatus.BAD_REQUEST , "Invalid category data" , e );
		}
	}

	@DeleteMapping("/{id}")
	public void deleteCategoryById(@PathVariable Long id) {
		categoryRepository.deleteById( id );
	}
}
