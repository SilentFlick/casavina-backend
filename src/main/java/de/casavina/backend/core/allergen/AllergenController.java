package de.casavina.backend.core.allergen;

import de.casavina.backend.model.Allergen;
import jakarta.validation.Valid;
import jakarta.validation.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping(value = "/v1/allergens", produces = "application/json")
public class AllergenController {

	private final AllergenRepository allergenRepository;

	public AllergenController(AllergenRepository allergenRepository) {
		this.allergenRepository = allergenRepository;
	}

	@GetMapping
	public Iterable<Allergen> getAllAllergen() {
		return allergenRepository.findAll( );
	}

	@GetMapping("/{id}")
	public Allergen getAllergenById(@PathVariable Long id) {
		return allergenRepository.findById( id )
				.orElseThrow(
						() -> new ResponseStatusException( HttpStatus.NOT_FOUND , "Allergen " + id + " not found" ) );
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Map<String, Long> createAllergen(@Valid @RequestBody Allergen allergen) {
		try {
			Allergen savedAllergen = allergenRepository.save( allergen );
			return Collections.singletonMap( "id" , savedAllergen.getId( ) );
		} catch (ValidationException e) {
			throw new ResponseStatusException( HttpStatus.BAD_REQUEST , "Invalid allergen data" , e );
		}
	}

	@PutMapping
	public void updateAllergen(@Valid @RequestBody Allergen allergen) {
		try {
			allergenRepository.findById( allergen.getId( ) )
					.map( existingAllergen -> {
						existingAllergen.setName( allergen.getName( ) );
						existingAllergen.setShortName( allergen.getShortName( ) );
						return allergenRepository.save( existingAllergen );
					} )
					.orElseThrow( () -> new ResponseStatusException( HttpStatus.NOT_FOUND ,
							"Allergen " + allergen.getId( ) + " not found" ) );
		} catch (ValidationException e) {
			throw new ResponseStatusException( HttpStatus.BAD_REQUEST , "Invalid allergen data" , e );
		}
	}

	@DeleteMapping("/{id}")
	public void deleteAllergen(@PathVariable Long id) {
		allergenRepository.deleteById( id );
	}
}
