package de.casavina.backend.security;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.constraints.NotNull;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationFailureListener
		implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {
	private final HttpServletRequest httpServletRequest;
	private final BruteForceProtectionService loginAttemptService;

	public AuthenticationFailureListener(HttpServletRequest httpServletRequest ,
	                                     BruteForceProtectionService loginAttemptService) {
		this.httpServletRequest = httpServletRequest;
		this.loginAttemptService = loginAttemptService;
	}

	@Override
	public void onApplicationEvent(@NotNull AuthenticationFailureBadCredentialsEvent event) {
		final String xfHeader = httpServletRequest.getHeader( "X-Forwarded-For" );
		if ( xfHeader == null || xfHeader.isEmpty( ) ||
				!xfHeader.contains( httpServletRequest.getRemoteAddr( ) ) ) {
			loginAttemptService.loginFailed( httpServletRequest.getRemoteAddr( ) );
		} else {
			loginAttemptService.loginFailed( xfHeader.split( "," )[0] );
		}
	}
}
