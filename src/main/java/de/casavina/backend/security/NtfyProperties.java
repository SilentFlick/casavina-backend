package de.casavina.backend.security;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "ntfy")
public record NtfyProperties(String host , String token) {
}
