package de.casavina.backend.security;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
public class BruteForceProtectionService {
	private final ConcurrentHashMap<String, Integer> attemptsCache;
	private final ConcurrentHashMap<String, Instant> expiredBlockedClients;
	private final HttpServletRequest request;

	public BruteForceProtectionService(HttpServletRequest request) {
		this.request = request;
		attemptsCache = new ConcurrentHashMap<>( 1000 );
		expiredBlockedClients = new ConcurrentHashMap<>( 1000 );
	}

	public void loginFailed(final String ip) {
		int attempts;
		Instant blockedTime;
		try {
			attempts = attemptsCache.get( ip );
			blockedTime = expiredBlockedClients.get( ip );
		} catch (NullPointerException npe) {
			attempts = 0;
			blockedTime = Instant.now( );
		}
		attempts++;
		attemptsCache.put( ip , attempts );
		expiredBlockedClients.put( ip , blockedTime.plus( 12 , ChronoUnit.HOURS ) );
		log.info( "IP " + ip + " attempts to login failed." );
	}

	public void clearAttemptCache() {
		Instant now = Instant.now( );
		for (String client : expiredBlockedClients.keySet( )) {
			Instant expirationTime = expiredBlockedClients.get( client );
			if ( now.isAfter( expirationTime ) ) {
				attemptsCache.remove( client );
				expiredBlockedClients.remove( client );
			}
		}
	}

	public boolean isClientBlocked() {
		int MAX_ATTEMPT = 10;
		try {
			int numberOfAttempts = attemptsCache.get( getClientIp( ) );
			return numberOfAttempts >= MAX_ATTEMPT;
		} catch (NullPointerException npe) {
			return false;
		}
	}

	private String getClientIp() {
		final String xfHeader = request.getHeader( "X-Forwarded-For" );
		if ( xfHeader != null ) {
			return xfHeader.split( "," )[0];
		}
		return request.getRemoteAddr( );
	}
}
