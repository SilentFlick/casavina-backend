package de.casavina.backend.security;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix = "cors")
public record CorsConfigurationProperties(List<String> origins , List<String> headers , List<String> methods) {
}
