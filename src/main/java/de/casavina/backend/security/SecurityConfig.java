package de.casavina.backend.security;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.sql.DataSource;
import java.util.concurrent.Executor;

import static org.springframework.security.config.Customizer.withDefaults;

@EnableWebSecurity
@EnableAsync
@Configuration
public class SecurityConfig {
	private final RsaKeyProperties rsaKeyProperties;
	private final CorsConfigurationProperties corsConfigurationProperties;

	public SecurityConfig(RsaKeyProperties rsaKeyProperties ,
	                      CorsConfigurationProperties corsConfigurationProperties) {
		this.rsaKeyProperties = rsaKeyProperties;
		this.corsConfigurationProperties = corsConfigurationProperties;
	}

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http , JwtDecoder jwtDecoder)
			throws Exception {
		http
				.csrf( AbstractHttpConfigurer::disable )
				.cors( withDefaults( ) )
				.authorizeHttpRequests( (authorize) -> authorize
						.requestMatchers( HttpMethod.POST , "/v1/auth/login" ).permitAll( )
						.requestMatchers( HttpMethod.GET , "/v1/menus/**" , "/v1/events/**" ,
								"/v1/categories/**" , "/v1/allergens/**" , "/v1/ingredients/**" ,
								"/v1/offers/**" ).permitAll( )
						.requestMatchers( HttpMethod.POST , "/v1/reservations" ).permitAll( )
						.requestMatchers( HttpMethod.PATCH , "/v1/users" ).hasAnyAuthority( "SCOPE_ROLE_USER" )
						.requestMatchers( "/v1/users/**" ).hasAnyAuthority( "SCOPE_ROLE_ADMIN" )
						.requestMatchers( "/actuator/health" ).permitAll( )
						.requestMatchers( "/error" ).permitAll( )
						.anyRequest( ).authenticated( )
				)
				.sessionManagement( (session) -> session
						.sessionCreationPolicy( SessionCreationPolicy.STATELESS ) )
				.oauth2ResourceServer( (oauth2) -> oauth2
						.jwt( (jwt) -> jwt.decoder( jwtDecoder ) ) );
		return http.build( );
	}

	@Bean
	public JwtDecoder jwtDecoder() {
		return NimbusJwtDecoder.withPublicKey( rsaKeyProperties.publicKey( ) ).build( );
	}

	@Bean
	public JwtEncoder jwtEncoder() {
		JWK jwk = new RSAKey.Builder( rsaKeyProperties.publicKey( ) ).privateKey(
				rsaKeyProperties.privateKey( ) ).build( );
		JWKSource<SecurityContext> jks = new ImmutableJWKSet<>( new JWKSet( jwk ) );
		return new NimbusJwtEncoder( jks );
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder( );
	}

	@Bean
	public UserDetailsManager users(DataSource dataSource , Environment environment) {
		String password = environment.getProperty( "spring.security.user.password" );
		String username = environment.getProperty( "spring.security.user.name" );
		assert username != null;
		JdbcUserDetailsManager users = new JdbcUserDetailsManager( dataSource );
		if ( !users.userExists( username ) ) {
			UserDetails admin = User.builder( )
					.username( username )
					.password( passwordEncoder( ).encode( password ) )
					.roles( "USER" , "ADMIN" )
					.build( );
			users.createUser( admin );
		}
		return users;
	}

	@Bean
	public AuthenticationManager authenticationManager(
			AuthenticationConfiguration authenticationConfiguration) throws Exception {
		return authenticationConfiguration.getAuthenticationManager( );
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configurationSource = new CorsConfiguration( );
		configurationSource.setAllowedOrigins( corsConfigurationProperties.origins( ) );
		configurationSource.setAllowedMethods( corsConfigurationProperties.methods( ) );
		configurationSource.setAllowedHeaders( corsConfigurationProperties.headers( ) );
		configurationSource.setAllowCredentials( true );
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource( );
		source.registerCorsConfiguration( "/**" , configurationSource );
		return source;
	}

	@Bean
	public Executor taskExecutor() {
		ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor( );
		threadPoolTaskExecutor.setCorePoolSize( 2 );
		threadPoolTaskExecutor.setMaxPoolSize( 2 );
		threadPoolTaskExecutor.setQueueCapacity( 500 );
		threadPoolTaskExecutor.setThreadNamePrefix( "CasaVina-TaskExecutor-" );
		threadPoolTaskExecutor.initialize( );
		return threadPoolTaskExecutor;
	}
}
