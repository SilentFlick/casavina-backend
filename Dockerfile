FROM openjdk:17-bullseye

ARG version

LABEL authors="Tan Minh Ho"
LABEL version=$version
LABEL description="Casa Vina Backend"

ENV SPING_PROFILES_ACTIVE=production
COPY target/backend-$version.jar app.jar
HEALTHCHECK --interval=5s --timeout=3s CMD curl -f http://localhost:8080/actuator/health || exit 1
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app.jar"]

MAINTAINER Tan Minh Ho <contact@tanminhho.com>